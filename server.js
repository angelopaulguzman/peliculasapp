const express = require('express');
const path = require('path');

const app = express();

app.use(express.static(__dirname + '/dist/peliculasApp'));

app.get('/*', function(req, res){
  res.sendFile(path.join( __dirname, 'dist/peliculasApp', 'index.html'));

});

app.listen({
  port: process.env.PORT || 8080
},
  ()=> console.log('Servidor http://localhost:8080')
  );
